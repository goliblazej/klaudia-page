import React from "react";
import styled from "styled-components";
import { Router, Switch, Route, Redirect } from "react-router-dom";
import { createBrowserHistory } from "history";
import AboutPage from "../pages/AboutPage";
import PortfolioPage from "../pages/PortfolioPage";
import ContactPage from "../pages/ContactPage";
import Header from "../components/Header";
import Menu from "../components/Menu";
import GalleryPage from "../pages/GalleryPage";
import FashionPage from "../pages/portfolio/FashionPage";
import GraphicsPage from "../pages/portfolio/GraphicsPage";
import PhotographyPage from "../pages/portfolio/PhotographyPage";
import FashionProjectsPage from "../pages/portfolio/FashionProjectsPage";
import GraphicsProjectsPage from "../pages/portfolio/GraphicsProjectsPage";
import PhotographyProjectsPage from "../pages/portfolio/PhotographyProjectsPage";

export const history = createBrowserHistory();

const AppRouter = () => (
  <Router history={history}>
    <Switch>
      <Container>
        <Header history={history} />
        <Menu history={history} />
        <Route path="/about" component={AboutPage} />
        <Route path="/portfolio" component={PortfolioPage} exact />
        <Route path="/portfolio/fashion" component={FashionPage} exact />
        <Route
          path="/portfolio/fashion/:project"
          component={FashionProjectsPage}
        />
        <Route path="/portfolio/graphics" component={GraphicsPage} exact />
        <Route
          path="/portfolio/graphics/:project"
          component={GraphicsProjectsPage}
        />
        <Route
          path="/portfolio/photography"
          component={PhotographyPage}
          exact
        />
        <Route
          path="/portfolio/photography/:project"
          component={PhotographyProjectsPage}
        />
        <Route path="/contact" component={ContactPage} />
        <Route path="/gallery" component={GalleryPage} />
        <Redirect exact from="/" to="/about" />
      </Container>
    </Switch>
  </Router>
);

export default AppRouter;

const Container = styled.div`
  background: white;
  color: grey;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
