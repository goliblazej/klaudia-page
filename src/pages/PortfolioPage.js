import React from "react";
import styled from "styled-components";
import photography from "../images/photography.jpg";
import graphicsdesign from "../images/graphicsdesign.jpg";
import fashion from "../images/fashion.png";

const PortfolioPage = props => (
  <Container>
    <Section onClick={() => props.history.push("/portfolio/photography")}>
      <PhotoContainer>
        <Photo src={photography} />
      </PhotoContainer>
      <Text>photography and fine arts</Text>
    </Section>

    <Section onClick={() => props.history.push("portfolio/graphics")}>
      <PhotoContainer>
        <Photo src={graphicsdesign} />
      </PhotoContainer>
      <Text>graphics and branding design</Text>
    </Section>

    <Section onClick={() => props.history.push("portfolio/fashion")}>
      <PhotoContainer>
        <Photo src={fashion} />
      </PhotoContainer>
      <Text>fashion</Text>
    </Section>
  </Container>
);

export default PortfolioPage;

const Container = styled.div`
  width: 100%;
  max-width: 920px;
  height: 450px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const Section = styled.div`
  position: relative;
  width: 21%;
  display: flex;
  flex-direction: column;

  &:hover {
    opacity: 0.7;
  }

  transition: all 0.3s linear;
`;

const PhotoContainer = styled.div`
  position: relative;
  height: 388px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Text = styled.div`
  text-transform: uppercase;
  height: 32px;
  text-align: center;
`;

const Photo = styled.img`
  position: relative;
  height: 80%;
`;
