import React from "react";
import styled from "styled-components";

const GalleryPage = () => <Content>Gallery</Content>;

export default GalleryPage;

const Content = styled.div`
  width: 100%;
  max-width: 900px;
  height: 400px;
  display: flex;
  flex-direction: row;
`;
