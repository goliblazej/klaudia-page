import React from "react";
import styled from "styled-components";
import EyesOpen from "../components/EyesOpen";
import EyesClosed from "../components/EyesClosed";

class ContactPage extends React.Component {
  state = {
    hover: false
  };

  render() {
    return (
      <Content>
        <PhotoContainer
          onMouseEnter={() => this.setState({ hover: true })}
          onMouseLeave={() => this.setState({ hover: false })}
        >
          {this.state.hover ? <EyesOpen /> : <EyesClosed />}
        </PhotoContainer>
        <TextContainer>
          <TextHeder>get in touch</TextHeder>
          <Text>
            Feel free to contact me if you have any question or comment. I am
            open for feedbacks, offers and collaborations. <br />
            <br />
            <br />
            <br />
            <br />
            klaudia.miedz@gmail.com
          </Text>
        </TextContainer>
      </Content>
    );
  }
}

export default ContactPage;

const Content = styled.div`
  width: 790px;
  height: 400px;
  display: flex;
  flex-direction: row;
  padding-top: 5px;
`;

const TextContainer = styled.div`
  position: relative;
  flex: 1;
  padding-left: 40px;
  text-align: right;
`;

const PhotoContainer = styled.div`
  position: relative;
  width: 60%;
  right: 41px;
  top: 9px;
`;

const TextHeder = styled.div`
  padding-bottom: 16px;
  font-size: 22px;
  font-weight: 700;
  font-family: PlayfairDisplay-Regular;
`;

const Text = styled.div`
  font-family: Simplifica;
  font-size: 18px;
  /* text-align: justify; */
  letter-spacing: calc(0.022 * 1em);
  line-height: 28px;
  color: rgba(0, 0, 0, 0.9);
`;
