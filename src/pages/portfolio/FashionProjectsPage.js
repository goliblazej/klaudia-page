import React from "react";
import Lgbt from "../../components/fashionProjects/Lgbt";

const FashionProjectsPage = props => {
  console.log(props.match.params.project);

  switch (props.match.params.project) {
    case "lgbt":
      return <Lgbt />;
    default:
      return <div />;
  }
};

export default FashionProjectsPage;
