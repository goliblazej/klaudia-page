import React from "react";
import styled from "styled-components";
import Card from "../../components/Card";
import lgbt1 from "../../images/lgbt/Lgbt1.jpg";

const FashionPage = props => {
  const navigateTo = props.history.push;

  return (
    <Container>
      <Row>
        <Card
          onClick={() => navigateTo("/portfolio/fashion/lgbt")}
          photo={lgbt1}
          label={"lgbt history month badges"}
          imgWidth={550}
          imgTop={-303}
          imgLeft={-970}
        />

        {/* <Card
          onClick={() => navigateTo("/portfolio/fashion/examplefashion")}
          label={"cardLabel"}
        />

        <Card
          onClick={() => navigateTo("/portfolio/fashion/examplefashion")}
          label={"cardLabel"}
        />
      </Row>
      <Row>
        <Card
          onClick={() => navigateTo("/portfolio/fashion/examplefashion")}
          label={"cardLabel"}
        /> */}
        <EmptyCard />
        <EmptyCard />
      </Row>
    </Container>
  );
};

export default FashionPage;

const Container = styled.div`
  width: 800px;
  height: 450px;
  display: flex;
  flex-direction: column;
`;

const Row = styled.div`
  height: 220px;
  margin-bottom: 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const EmptyCard = styled.div`
  background: transparent;
  width: 220px;
  height: 220px;
  position: relative;
`;
