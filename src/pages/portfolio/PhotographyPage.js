import React from "react";
import styled from "styled-components";
import Card from "../../components/Card";
import DRAWINGS1 from "../../images/sketchbook1/DRAWINGS-01.jpg";
import SB1 from "../../images/sketchbook2/sb1.jpg";
import P1 from "../../images/prp/p1.jpg";

const PhotographyPage = props => {
  const navigateTo = props.history.push;

  return (
    <Container>
      <Row>
        <Card
          onClick={() => navigateTo("/portfolio/photography/sketchbook1")}
          photo={DRAWINGS1}
          label={"sketchbook1"}
          imgWidth={239}
          imgTop={-125}
          imgLeft={-268}
        />
        <Card
          onClick={() => navigateTo("/portfolio/photography/sketchbook2")}
          photo={SB1}
          label={"sketchbook2"}
          imgWidth={271}
          imgTop={-179}
          imgLeft={-339}
        />
        <Card
          onClick={() => navigateTo("/portfolio/photography/photoreportprague")}
          photo={P1}
          label={"photo report - Prague"}
          imgWidth={271}
          imgTop={-179}
          imgLeft={-339}
        />
      </Row>
    </Container>
  );
};

export default PhotographyPage;

const Container = styled.div`
  width: 800px;
  height: 450px;
  display: flex;
  flex-direction: column;
`;

const Row = styled.div`
  height: 220px;
  margin-bottom: 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const EmptyCard = styled.div`
  background: transparent;
  width: 220px;
  height: 220px;
  position: relative;
`;
