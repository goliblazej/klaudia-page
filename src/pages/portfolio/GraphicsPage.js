import React from "react";
import styled from "styled-components";
import Card from "../../components/Card";
import YCD3 from "../../images/ycdproject/YCD3.jpg";
import ps1 from "../../images/ps/ps1.jpg";
import rm1 from "../../images/rm/rm1.jpg";
import a1 from "../../images/adidas/a1.jpg";
import rsa2 from "../../images/rsa/rsa2.jpg";

const GraphicsPage = props => {
  const navigateTo = props.history.push;

  return (
    <Container>
      <Row>
        <Card
          onClick={() => navigateTo("/portfolio/graphics/ycd")}
          photo={YCD3}
          label={" the future imagined project"}
          imgWidth={298}
          imgTop={-104}
          imgLeft={-87}
        />
        <Card
          onClick={() => navigateTo("/portfolio/graphics/pstates")}
          photo={ps1}
          label={"psychical states - posters"}
          imgWidth={740}
          imgTop={-141}
          imgLeft={-514}
        />

        <Card
          onClick={() => navigateTo("/portfolio/graphics/rm")}
          photo={rm1}
          label={"romeo and juliet - posters"}
          imgWidth={405}
          imgTop={-181}
          imgLeft={-518}
        />
      </Row>
      <Row>
        <Card
          onClick={() => navigateTo("/portfolio/graphics/adidas")}
          photo={a1}
          label={"adidas brief - campaign"}
          imgWidth={355}
          imgTop={-99}
          imgLeft={-37}
        />
        <Card
          onClick={() => navigateTo("/portfolio/graphics/rsa")}
          photo={rsa2}
          label={"rsa competition brief"}
          imgWidth={394}
          imgTop={-101}
          imgLeft={-58}
        />
        <EmptyCard />
      </Row>
    </Container>
  );
};

export default GraphicsPage;

const Container = styled.div`
  width: 800px;
  height: 450px;
  display: flex;
  flex-direction: column;
`;

const Row = styled.div`
  height: 220px;
  margin-bottom: 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const EmptyCard = styled.div`
  background: transparent;
  width: 220px;
  height: 220px;
  position: relative;
`;
