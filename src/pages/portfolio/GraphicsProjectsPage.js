import React from "react";
import Ycd from "../../components/graphicsProjects/Ycd";
import Pstates from "../../components/graphicsProjects/Pstates";
import Rm from "../../components/graphicsProjects/Rm";
import Adidas from "../../components/graphicsProjects/Adidas";
import Rsa from "../../components/graphicsProjects/Rsa";

const GraphicsProjectsPage = props => {
  console.log(props.match.params.project);

  switch (props.match.params.project) {
    case "ycd":
      return <Ycd />;
    case "pstates":
      return <Pstates />;
    case "rm":
      return <Rm />;
    case "adidas":
      return <Adidas />;
    case "rsa":
      return <Rsa />;
    default:
      return <div />;
  }
};

export default GraphicsProjectsPage;
