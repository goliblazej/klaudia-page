import React from "react";
import Sketchbook1 from "../../components/photographyProjects/Sketchbook1";
import Sketchbook2 from "../../components/photographyProjects/Sketchbook2";
import PhotoreportPrague from "../../components/photographyProjects/PhotoreportPrague";

const PhotographyProjectsPage = props => {
  console.log(props.match.params.project);

  switch (props.match.params.project) {
    case "sketchbook1":
      return <Sketchbook1 />;
    case "sketchbook2":
      return <Sketchbook2 />;
    case "photoreportprague":
      return <PhotoreportPrague />;
    default:
      return <div />;
  }
};

export default PhotographyProjectsPage;
