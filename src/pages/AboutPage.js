import React from "react";
import styled from "styled-components";
import faces from "../images/faces.jpg";

const AboutPage = props => (
  <Content>
    <TextContainer>
      <TextHeder>Klaudia Miedzinska</TextHeder>
      <Text>
        I am freshly graduated student passionate about the clean, simple
        design. I am open person liking to work individually as well as within
        the team. I managed to complete two internships so far which gave me
        basic recognition about industry and clients needs. I have a small crush
        on a precursor of a poster - Lautrec the Toulouse. I chose my pathway to
        be an artist when I was really young, started attending classes when I
        was 5 and not stopping my education ever since. I am feeling the most
        comfortable working in Illustrator, but it didn’t stop me to learn
        Photoshop, InDesign and basics of Adobe Premier Pro. I have knowledge
        about fine arts, sculpture, history of art, print media and much more.
        Currently, I am learning how to code.
      </Text>
    </TextContainer>
    <PhotoContainer>
      <Photo src={faces} />
    </PhotoContainer>
  </Content>
);

export default AboutPage;

const Content = styled.div`
  width: 790px;
  height: 400px;
  display: flex;
  flex-direction: row;
`;

const TextContainer = styled.div`
  position: relative;
  flex: 1;
  padding-right: 26px;
`;

const PhotoContainer = styled.div`
  position: relative;
  width: 60%;
`;

const TextHeder = styled.div`
  padding-bottom: 16px;
  font-size: 22px;
  font-weight: 700;
  font-family: PlayfairDisplay-Regular;
`;

const Text = styled.div`
  font-family: Simplifica;
  font-size: 14px;
  /* text-align: justify; */
  letter-spacing: calc(0.022 * 1em);
  line-height: 28px;
  color: rgba(0, 0, 0, 0.9);
`;

const Photo = styled.img`
  position: relative;
  width: 90%;
  margin-left: 10%;
  margin-top: 3%;
`;
