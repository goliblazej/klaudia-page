import React from "react";
import styled from "styled-components";
import logo from "../images/logo.jpg";

const Header = props => (
  <Container>
    <Logo src={logo} onClick={() => props.history.push("/about")} />
  </Container>
);

export default Header;

const Container = styled.div`
  position: relative;
  width: 100%;
  height: 230px;
  /* border: 1px solid green; */
  display: flex;
  align-items: flex-end;
  justify-content: center;
`;

const Logo = styled.img`
  position: relative;
  width: 200px;
  height: 170px;
  /* border: 1px solid green; */

  &:hover {
    opacity: 0.7;
  }

  transition: all 0.3s linear;
`;
