import React from "react";
import styled from "styled-components";
import { withRouter } from "react-router";

const Menu = props => {
  const pathName = props.history.location.pathname;
  let isFashion = false;
  let isGraphics = false;
  let isPhotography = false;
  if (pathName.split("/").length > 2) {
    isFashion = pathName.split("/")[2] === "fashion";
    isGraphics = pathName.split("/")[2] === "graphics";
    isPhotography = pathName.split("/")[2] === "photography";
  }
  return (
    <Container>
      <Element
        align={"flex-start"}
        onClick={() => props.history.push("/about")}
      >
        About
      </Element>
      <PortfolioElement>
        <Link onClick={() => props.history.push("/portfolio")}>Portfolio</Link>
        <CategoryLink onClick={() => props.history.push("/portfolio/fashion")}>
          {isFashion && "Fashion"}
        </CategoryLink>
        <CategoryLink onClick={() => props.history.push("/portfolio/graphics")}>
          {isGraphics && "graphics and branding design"}
        </CategoryLink>
        <CategoryLink
          onClick={() => props.history.push("/portfolio/photography")}
        >
          {isPhotography && "photography and fine arts"}
        </CategoryLink>
      </PortfolioElement>
      <Element
        align={"flex-end"}
        onClick={() => props.history.push("/contact")}
      >
        Contact
      </Element>
    </Container>
  );
};

export default withRouter(Menu);

const Container = styled.div`
  width: 790px;
  height: 150px;
  display: flex;
  position: relative;
  justify-content: space-between;
  /* font-family: PantonDemo-Light; */
  font-size: 18px;
  /* text-align: justify; */
  letter-spacing: calc(0.09 * 1em);
  color: rgba(0, 0, 0, 0.9);
`;

const Element = styled.div`
  text-transform: uppercase;
  display: flex;
  align-items: center;
  width: 200px;
  justify-content: ${props => props.align};

  &:hover {
    opacity: 0.7;
    font-size: 20px;
  }

  transition: all 0.3s linear;
`;

const PortfolioElement = styled.div`
  text-transform: uppercase;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const CategoryLink = styled.div`
  position: absolute;
  top: 90px;
  font-size: 12px;

  &:hover {
    font-size: 15px;
    opacity: 0.7;
  }

  transition: all 0.3s linear;
`;

const Link = styled.div`
  height: 30px;
  position: relative;

  &:hover {
    opacity: 0.7;
    font-size: 20px;
  }

  transition: all 0.3s linear;
`;
