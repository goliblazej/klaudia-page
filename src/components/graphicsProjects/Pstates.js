import React from "react";
import styled from "styled-components";
import ps1 from "../../images/ps/ps1.jpg";
import ps2 from "../../images/ps/ps2.jpg";

const Pstates = () => (
  <Content>
    <TextContainer>
      <TextHeder>Psychical States - Posters </TextHeder>
      <Text>
        Psychical States poster is my first task at the University. I had to
        focus on my feelings which I had at this moment, find a synonym to one
        of my feelings and develop it into the poster. I felt relaxed, free,
        safe, so I chose the word ‘relax’ and find the best synonym back then
        ‘repose’. When you want to calm down you are breathing, I wanted to
        create something, simple, light and don’t be afraid of space.
      </Text>
    </TextContainer>
    <PhotoContainer>
      <Photo src={ps1} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={ps2} />
    </PhotoContainer>
  </Content>
);

export default Pstates;

const Content = styled.div`
  width: 790px;
  /* height: 400px; */
  display: flex;
  flex-direction: column;
`;

const TextContainer = styled.div`
  position: relative;
  width: 100%;
  text-align: center;
  margin-bottom: 40px;
`;

const PhotoContainer = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 40px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
`;
const TextHeder = styled.div`
  padding-bottom: 16px;
  font-size: 22px;
  font-weight: 700;
  font-family: PlayfairDisplay-Regular;
`;
const Photo = styled.img`
  position: relative;
  width: 100%;
`;
const Text = styled.div`
  font-family: Simplifica;
  font-size: 16px;
  letter-spacing: calc(0.022 * 1em);
  line-height: 28px;
  color: rgba(0, 0, 0, 0.9);
`;
