import React from "react";
import styled from "styled-components";
import a1 from "../../images/adidas/a1.jpg";
import a2 from "../../images/adidas/a2.jpg";

const Adidas = () => (
  <Content>
    <TextContainer>
      <TextHeder>Adidas Brief - Campaign</TextHeder>
      <Text>
        Adidas Brief was part of the D&AD competition. Brief focused on problem
        – solving the situation. The main point was to create a campaign for
        Adidas using one or all of the cities from the brief focusing on the
        particular cities’ issues. I focused on the fast-developing environment
        which is causing for example lack of green spaces in the cities (parks,
        squares) where people usually are working out. Also, I thought about
        finance points that not everyone can pay for gym subscription as well as
        time issue, people are usually really busy so they need really easy
        access to work out. I focused mainly on young adults so the overall of
        the campaign is kept modern and suitable to the target audience. My idea
        was ‘vertical running’. Adidas is already one of the sponsors for few of
        ‘Vertical World Circuit’ participants. Vertical running is running up
        the stairs which means that it is easy to use most of the already
        existing locations. Also to promote this discipline and have in mind a
        lack of green spaces I decided to create vertical gardens to promote
        Adidas.
      </Text>
    </TextContainer>
    <PhotoContainer>
      <Photo src={a1} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={a2} />
    </PhotoContainer>
  </Content>
);

export default Adidas;

const Content = styled.div`
  width: 790px;
  /* height: 400px; */
  display: flex;
  flex-direction: column;
`;

const TextContainer = styled.div`
  position: relative;
  width: 100%;
  text-align: center;
  margin-bottom: 40px;
`;

const PhotoContainer = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 40px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
`;
const TextHeder = styled.div`
  padding-bottom: 16px;
  font-size: 22px;
  font-weight: 700;
  font-family: PlayfairDisplay-Regular;
`;
const Photo = styled.img`
  position: relative;
  width: 100%;
`;
const Text = styled.div`
  font-family: Simplifica;
  font-size: 16px;
  letter-spacing: calc(0.022 * 1em);
  line-height: 28px;
  color: rgba(0, 0, 0, 0.9);
`;
