import React from "react";
import styled from "styled-components";
import YCD1 from "../../images/ycdproject/YCD1.jpg";
import YCD2 from "../../images/ycdproject/YCD2.jpg";
import YCD3 from "../../images/ycdproject/YCD3.jpg";
import YCD4 from "../../images/ycdproject/YCD4.jpg";
import YCD5 from "../../images/ycdproject/YCD5.jpg";

const Ycd = () => (
  <Content>
    <TextContainer>
      <TextHeder>The Future Imagined</TextHeder>
      <Text>
        The future Imagined brief was focused on creating a product or service
        which doesn’t exist and create a brand for it. My idea was the clothing
        machine. Small, simple to use, cheap. I focused on three target
        audiences: -young people – to bring up their creativity -parents and
        kids – to make their time with kids special -elderly people – to bring
        up their memories I created the logo, handbook with information about
        fabrics and the advert in the magazine as representative parts of the
        project.
      </Text>
    </TextContainer>
    <PhotoContainer>
      <Photo src={YCD1} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={YCD2} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={YCD3} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={YCD4} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={YCD5} />
    </PhotoContainer>
  </Content>
);

export default Ycd;

const Content = styled.div`
  width: 790px;
  /* height: 400px; */
  display: flex;
  flex-direction: column;
`;

const TextContainer = styled.div`
  position: relative;
  width: 100%;
  text-align: center;
  margin-bottom: 40px;
`;

const PhotoContainer = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 40px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
`;
const TextHeder = styled.div`
  padding-bottom: 16px;
  font-size: 22px;
  font-weight: 700;
  font-family: PlayfairDisplay-Regular;
`;
const Photo = styled.img`
  position: relative;
  width: 100%;
`;
const Text = styled.div`
  font-family: Simplifica;
  font-size: 16px;
  letter-spacing: calc(0.022 * 1em);
  line-height: 28px;
  color: rgba(0, 0, 0, 0.9);
`;
