import React from "react";
import styled from "styled-components";
import rm1 from "../../images/rm/rm1.jpg";
import rm2 from "../../images/rm/rm2.jpg";

const Rm = () => (
  <Content>
    <TextContainer>
      <TextHeder>Romeo and Juliet - Posters</TextHeder>
      <Text>
        Romeo and Juliet was a task in small teams. We worked within photography
        and graphic design student. The purpose of it was to create posters for
        play. In our case, it was “Romeo and Juliet” poster for National
        Theatre. We wanted to keep the character of the institution and show the
        modern interpretation of Romeo and Juliet. Keeping in mind our main idea
        each of us was supposed to create own version of the poster. This task
        allowed us to enhance our Art Direction and Teamwork skills.
      </Text>
    </TextContainer>
    <PhotoContainer>
      <Photo src={rm1} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={rm2} />
    </PhotoContainer>
  </Content>
);

export default Rm;

const Content = styled.div`
  width: 790px;
  /* height: 400px; */
  display: flex;
  flex-direction: column;
`;

const TextContainer = styled.div`
  position: relative;
  width: 100%;
  text-align: center;
  margin-bottom: 40px;
`;

const PhotoContainer = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 40px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
`;
const TextHeder = styled.div`
  padding-bottom: 16px;
  font-size: 22px;
  font-weight: 700;
  font-family: PlayfairDisplay-Regular;
`;
const Photo = styled.img`
  position: relative;
  width: 100%;
`;
const Text = styled.div`
  font-family: Simplifica;
  font-size: 16px;
  letter-spacing: calc(0.022 * 1em);
  line-height: 28px;
  color: rgba(0, 0, 0, 0.9);
`;
