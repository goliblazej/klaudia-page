import React from "react";
import styled from "styled-components";
import RSA1 from "../../images/rsa/rsa1.jpg";
import RSA2 from "../../images/rsa/rsa2.jpg";
import RSA3 from "../../images/rsa/rsa3.jpg";
import RSA4 from "../../images/rsa/rsa4.jpg";
import RSA5 from "../../images/rsa/rsa5.jpg";

const Rsa = () => (
  <Content>
    <TextContainer>
      <TextHeder>
        POLITICS**T - Informational and Entertainment Portal
      </TextHeder>
    </TextContainer>
    <PhotoContainer>
      <Photo src={RSA1} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={RSA2} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={RSA3} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={RSA4} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={RSA5} />
    </PhotoContainer>
  </Content>
);

export default Rsa;

const Content = styled.div`
  width: 790px;
  /* height: 400px; */
  display: flex;
  flex-direction: column;
`;

const TextContainer = styled.div`
  position: relative;
  width: 100%;
  text-align: center;
  margin-bottom: 40px;
`;

const PhotoContainer = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 40px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
`;
const TextHeder = styled.div`
  padding-bottom: 16px;
  font-size: 22px;
  font-weight: 700;
  font-family: PlayfairDisplay-Regular;
`;
const Photo = styled.img`
  position: relative;
  width: 100%;
`;
