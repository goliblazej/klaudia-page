import React from "react";
import styled from "styled-components";

class Card extends React.Component {
  state = {
    isHovering: false
  };

  render() {
    const { photo, label } = this.props;

    return (
      <Container
        onMouseEnter={() => this.setState({ isHovering: true })}
        onMouseLeave={() => this.setState({ isHovering: false })}
        onClick={this.props.onClick}
      >
        {photo && <CardBackround src={photo} {...this.props} />}
        {/* {this.state.isHovering && <Label>{label}</Label>} */}
        <Label>{label}</Label>
      </Container>
    );
  }
}

export default Card;

const Container = styled.div`
  width: 220px;
  height: 220px;
  background: grey;
  position: relative;
  overflow: hidden;

  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);

  &:hover {
    box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  }
`;

const Label = styled.div`
  padding: 16px;
  position: absolute;
  bottom: 0;
  width: calc(100% - 32px);

  background: grey;
  opacity: 0;

  ${Container}:hover & {
    opacity: 1;
    background: whitesmoke;
  }

  /* transition: opacity 0.3s ease-in; */
  /* transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1); */
  transition: all 0.5s ease-in-out;
`;

const CardBackround = styled.img`
  position: absolute;
  top: ${props => (props.imgTop ? props.imgTop : 0)}px;
  left: ${props => (props.imgLeft ? props.imgLeft : 0)}px;
  width: ${props => (props.imgWidth ? props.imgWidth : 300)}%;
`;
