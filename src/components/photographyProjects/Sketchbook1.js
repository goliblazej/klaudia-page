import React from "react";
import styled from "styled-components";
import IMAGE1 from "../../images/sketchbook1/DRAWINGS-01.jpg";
import IMAGE2 from "../../images/sketchbook1/DRAWINGS-02.jpg";
import IMAGE3 from "../../images/sketchbook1/DRAWINGS-03.jpg";
import IMAGE4 from "../../images/sketchbook1/DRAWINGS-04.jpg";
import IMAGE5 from "../../images/sketchbook1/DRAWINGS-05.jpg";
import IMAGE6 from "../../images/sketchbook1/DRAWINGS-06.jpg";
import IMAGE7 from "../../images/sketchbook1/DRAWINGS-07.jpg";

const Sketchbook1 = () => (
  <Content>
    <PhotoContainer>
      <Photo src={IMAGE1} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={IMAGE2} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={IMAGE3} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={IMAGE4} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={IMAGE5} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={IMAGE6} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={IMAGE7} />
    </PhotoContainer>
  </Content>
);

export default Sketchbook1;

const Content = styled.div`
  width: 790px;
  /* height: 400px; */
  display: flex;
  flex-direction: column;
`;

const PhotoContainer = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 40px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  background: black;
`;

const Photo = styled.img`
  position: relative;
  width: 100%;
`;
