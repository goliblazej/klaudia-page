import React from "react";
import styled from "styled-components";
import IMAGE1 from "../../images/prp/p1.jpg";
import IMAGE2 from "../../images/prp/p2.jpg";
import IMAGE3 from "../../images/prp/p3.jpg";
import IMAGE4 from "../../images/prp/p4.jpg";
import IMAGE5 from "../../images/prp/p5.jpg";

const PhotoreportPrague = () => (
  <Content>
    <TextContainer>
      <TextHeder>Photo Report - Trip to Prague</TextHeder>
    </TextContainer>
    <PhotoContainer>
      <Photo src={IMAGE1} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={IMAGE2} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={IMAGE3} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={IMAGE4} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={IMAGE5} />
    </PhotoContainer>
  </Content>
);

export default PhotoreportPrague;

const Content = styled.div`
  width: 790px;
  /* height: 400px; */
  display: flex;
  flex-direction: column;
`;

const PhotoContainer = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 40px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  background: black;
`;

const Photo = styled.img`
  position: relative;
  width: 100%;
`;

const TextContainer = styled.div`
  position: relative;
  width: 100%;
  text-align: center;
  margin-bottom: 40px;
`;
const TextHeder = styled.div`
  padding-bottom: 16px;
  font-size: 22px;
  font-weight: 700;
  font-family: PlayfairDisplay-Regular;
`;
