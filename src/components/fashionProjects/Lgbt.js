import React from "react";
import styled from "styled-components";
import lgbt1 from "../../images/lgbt/Lgbt1.jpg";
import lgbt2 from "../../images/lgbt/Lgbt2.jpg";
import lgbt3 from "../../images/lgbt/Lgbt3.jpg";

const Lgbt = () => (
  <Content>
    <TextContainer>
      <TextHeder>LGBT History Month Badges</TextHeder>
      <Text>
        For this project, I had to create a badge for the celebration of LGBT
        history month. The theme for these badges was geography. I decided to
        focus on current trends, significant motives of community, give them a
        bit of symbolism and also get to the target audience in the best
        possible way. I came up with three ideas.
      </Text>
    </TextContainer>
    <PhotoContainer>
      <Photo src={lgbt1} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={lgbt2} />
    </PhotoContainer>
    <PhotoContainer>
      <Photo src={lgbt3} />
    </PhotoContainer>
  </Content>
);

export default Lgbt;

const Content = styled.div`
  width: 790px;
  /* height: 400px; */
  display: flex;
  flex-direction: column;
`;

const TextContainer = styled.div`
  position: relative;
  width: 100%;
  text-align: center;
  margin-bottom: 40px;
`;

const PhotoContainer = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 40px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
`;
const TextHeder = styled.div`
  padding-bottom: 16px;
  font-size: 22px;
  font-weight: 700;
  font-family: PlayfairDisplay-Regular;
`;
const Photo = styled.img`
  position: relative;
  width: 100%;
`;
const Text = styled.div`
  font-family: Simplifica;
  font-size: 16px;
  letter-spacing: calc(0.022 * 1em);
  line-height: 28px;
  color: rgba(0, 0, 0, 0.9);
`;
