import React from "react";
import styled from "styled-components";
import { withRouter } from "react-router-dom";
import { useSpring, animated, config } from "react-spring";
import EyesClosed from "./EyesClosed";
import EyesOpen from "./EyesOpen";

const Animation = ({ children, delay, transform }) => {
  const props = useSpring({
    opacity: 1,
    transform: "translate3d(0,0,0)",
    from: {
      opacity: 0,
      transform: transform ? transform : "translate3d(0,50px,0)"
    },
    delay: delay,
    config: config.gentle
  });
  return <animated.div style={props}>{children}</animated.div>;
};

class Background extends React.Component {
  state = {
    hover: false
  };

  toggleHover = () => {
    this.setState({ hover: !this.state.hover });
  };

  render() {
    if (this.props.location.pathname === "/contact")
      return (
        <>
          <Animation delay={400} transform={"translate3d(0,50px,0)"}>
            <Header themecolor={this.props.themeColor}>Get in touch!</Header>
          </Animation>
          <Container>
            <HoverContainer
              onMouseEnter={this.toggleHover}
              onMouseLeave={this.toggleHover}
            >
              {this.state.hover ? <EyesOpen /> : <EyesClosed />}
            </HoverContainer>
          </Container>
          <Email>klaudia.miedz@gmail.com</Email>
        </>
      );
    return <BackgroundRegular />;
  }
}

export default withRouter(props => <Background {...props} />);

const BackgroundRegular = styled.div`
  width: calc(100% - 345px);
  height: 100vh;
  left: 345px;
  opacity: 0.8;
  position: absolute;
  background-image: url(./images/background.png);
  background-repeat: no-repeat;
  background-size: 100%;
  background-position: center;
  // filter: blur(20px);
`;

const Container = styled.div`
  width: calc(100% - 345px);
  top: 130px;
  left: 405px;
  position: absolute;
  display: flex;
  justify-content: center;
  min-width: 650px;
  min-height: 435px;
  opacity: 0.8;
`;

const HoverContainer = styled.div`
  position: relative;
  width: 700px;
  height: 435px;
  display: flex;
  justify-content: center;
`;

const Header = styled.div`
  width: calc(100% - 345px);
  top: 145px;
  left: 400px;
  position: absolute;
  text-align: center;
  height: 45px;
  line-height: 45px;
  font-size: 36px;
  font-weight: 500;
  color: ${props => props.themecolor};
  letter-spacing: 0.115em;
  min-width: 650px;
  min-height: 435px;
`;

const Email = styled.div`
  width: calc(100% - 345px);
  top: 590px;
  left: 400px;
  position: absolute;
  text-align: center;
  height: 28px;
  line-height: 28px;
  font-size: 24px;
  color: grey;
  font-weight: 700;
  min-width: 650px;
  min-height: 435px;
`;
